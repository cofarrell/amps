package ${package};

public interface MyPluginComponent
{
    String getName();
}
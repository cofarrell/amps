-s
${project.build.directory}/it/settings.xml
-Dproduct.version=${product.version}
-Dproduct.data.version=${product.data.version}
-Dhttp.port.bak=${it.http.port}
-Drmi.port.bak=${it.rmi.port}
-DtestGroup=bar
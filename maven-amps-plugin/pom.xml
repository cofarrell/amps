<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.atlassian.amps</groupId>
        <artifactId>atlassian-amps-parent</artifactId>
        <version>4.1.2-SNAPSHOT</version>
    </parent>

    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>maven-amps-plugin</artifactId>
    <packaging>maven-plugin</packaging>

    <name>Maven AMPS Plugin</name>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.core</groupId>
            <artifactId>atlassian-core</artifactId>
            <version>4.0</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.4</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.10</version>
        </dependency>
        <dependency>
            <groupId>biz.aQute</groupId>
            <artifactId>bndlib</artifactId>
            <version>1.43.0-atlassian-1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.ant</groupId>
            <artifactId>ant</artifactId>
            <version>1.7.1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-project</artifactId>
            <version>2.1.0</version>
        </dependency>
        <dependency>
            <!-- Version required by mojo-executor -->
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-utils</artifactId>
            <version>1.5.6</version>
        </dependency>

        <!-- These actually don't do anything, but still declared here for documentation purposes -->
        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <version>2.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-resources-plugin</artifactId>
            <version>2.3</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.4.3</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <version>2.2</version>
        </dependency>
        <dependency>
            <groupId>org.twdata.maven</groupId>
            <artifactId>maven-cli-plugin</artifactId>
            <version>0.7</version>
        </dependency>
        <dependency>
            <groupId>com.googlecode.guava-osgi</groupId>
            <artifactId>guava-osgi</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-compress</artifactId>
            <version>1.4.1</version>
        </dependency>

        <!-- needed for plugin module generation -->
        <dependency>
            <groupId>jdom</groupId>
            <artifactId>jdom</artifactId>
            <version>1.0</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.amps</groupId>
            <artifactId>plugin-module-codegen-maven-client</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- refapp archetype for integration testing -->
        <dependency>
            <groupId>com.atlassian.maven.archetypes</groupId>
            <artifactId>refapp-plugin-archetype</artifactId>
            <version>${project.version}</version>
            <scope>runtime</scope>
        </dependency>

        <!-- google tracking -->
        <dependency>
          <groupId>com.dmurph</groupId>
          <artifactId>JGoogleAnalyticsTracker</artifactId>
          <version>1.2.1-atlassian</version>
        </dependency>
        <dependency>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-nop</artifactId>
          <version>1.6.2</version>
        </dependency>

        <!-- rest doc generation -->
        <dependency>
          <groupId>com.atlassian.plugins.rest</groupId>
          <artifactId>atlassian-rest-doclet</artifactId>
          <version>2.5.0</version>
        </dependency>

        <dependency>
          <groupId>com.atlassian.amps</groupId>
          <artifactId>standalone</artifactId>
          <version>${project.version}</version>
          <type>pom</type>
          <scope>runtime</scope>
        </dependency>
        
        <!-- func test framework -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
        </dependency>
        
        <!-- minification -->
        <dependency>
            <groupId>com.google.javascript</groupId>
            <artifactId>closure-compiler</artifactId>
            <version>rr2079.1</version>
        </dependency>
        <dependency>
            <groupId>com.yahoo.platform.yui</groupId>
            <artifactId>yuicompressor</artifactId>
            <version>2.4.7</version>
        </dependency>
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
            <version>1.6</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven.shared</groupId>
            <artifactId>maven-common-artifact-filters</artifactId>
            <version>1.3</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <createDependencyReducedPom>true</createDependencyReducedPom>
                            <artifactSet>
                                <includes>
                                    <include>org.twdata.maven:mojo-executor</include>
                                </includes>
                            </artifactSet>
                            <transformers>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ComponentsXmlResourceTransformer" />
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>attached</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <outputDirectory>${project.build.directory}</outputDirectory>
                    <descriptors>
                        <descriptor>src/main/assembly/integration-tests.xml</descriptor>
                    </descriptors>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
